package model.vo;

public class VOEspecialService implements Comparable<VOEspecialService>
{

	private int id;
	private int date;
	private int exception_type;
	
	
	public VOEspecialService(int id, int date, int exception_type) {
	
		this.id = id;
		this.date = date;
		this.exception_type = exception_type;
	}


	public int compareTo(VOEspecialService o) {
		
			if(id < o.getId()) {
				return -1;}
			else if(id > o.getId()) {
				return 1;}
			else {
			return 0;}
		
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getDate() {
		return date;
	}


	public void setDate(int date) {
		this.date = date;
	}


	public int getException_type() {
		return exception_type;
	}


	public void setException_type(int exception_type) {
		this.exception_type = exception_type;
	}

}
