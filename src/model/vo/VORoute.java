package model.vo;

/**
 * Representation of a route object
 */
public class VORoute implements Comparable<VORoute>{

    // -----------------------------------------------------------------
    // Attributes
    // -----------------------------------------------------------------

    private int routeId;
    private String agencyId;
    private String routeShortName;
    private String routeLongName;
   

    // -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

    /**
     * @return id - Route's id number
     */
    public VORoute(int pRouteId , String pAgencyId, String pRouteShortName, String pRouteLongName)
    {
        routeId = pRouteId;
        agencyId = pAgencyId;
        routeShortName = pRouteShortName;
        routeLongName = pRouteLongName;
       
    }

    // -----------------------------------------------------------------
    // Methods - Getters
    // -----------------------------------------------------------------

    public int id() {

        return routeId;
    }
    
    public int hashCode()
    {
    		return routeShortName.hashCode();
    }

    /**
     * @return name - route name
     */
    public String getName() {

        return routeShortName;
    }

    public int getRouteId()
    {
        return routeId;
    }

    public String getAgencyId()
    {
        return agencyId;
    }

    public String getRouteShortName()
    {
        return routeShortName;
    }

    public String getRouteLongName()
    {
        return routeLongName;
    }

   

    // -----------------------------------------------------------------
    // Methods - Setters
    // -----------------------------------------------------------------

    public void setRouteId(int newValue)
    {
        routeId = newValue;
    }

    public void setAgencyId(String newValue)
    {
        agencyId = newValue;
    }

    public void setRouteShortName(String newValue)
    {
        routeShortName = newValue;
    }

    public void setRouteLongName(String newValue)
    {
        routeLongName = newValue;
    }

   
    // -----------------------------------------------------------------
    // Methods - Logic
    // -----------------------------------------------------------------

    public int compareTo(VORoute n) {
        int r;

        if(n.id() == routeId)
        {
            r=0;
        }
        else if(n.id() < routeId)
        {
            r=1;
        }
        else
        {
            r=-1;
        }

        return r;
    }
}
