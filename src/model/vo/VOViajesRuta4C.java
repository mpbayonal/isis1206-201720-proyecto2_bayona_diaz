package model.vo;
import model.data_structures.*;;


/**
 * Clase enfocada en el retorno del requerimiento 4C, que referencia la ruta que est� usando
 *  la parada compartida y los viajes de dicha ruta que la usan.
 * @author Daniel
 *
 */
public class VOViajesRuta4C implements Comparable<VOViajesRuta4C>{
	
	public VOViajesRuta4C(Integer idRuta, Integer idParadaCompartida) {
		super();
		this.idRuta = idRuta;
		this.idParadaCompartida = idParadaCompartida;
		this.viajes = new DoubleLinkedList<VOViaje>();
	}

	/**
	 * id de la ruta que usa la parada.
	 */
	private Integer idRuta;
	
	/**
	 * id de la Parada
	 */
	private Integer idParadaCompartida;
	
	/**
	 * Lista de los viajes de la ruta, que usan la parada.
	 */
	private DoubleLinkedList<VOViaje> viajes;

	public Integer getIdRuta() {
		return idRuta;
	}

	public void setIdRuta(int idRuta) {
		this.idRuta = idRuta;
	}

	public int getIdParadaCompartida() {
		return idParadaCompartida;
	}

	public void setIdParadaCompartida(int idParadaCompartida) {
		this.idParadaCompartida = idParadaCompartida;
	}

	public DoubleLinkedList<VOViaje> getViajes() {
		return viajes;
	}

	public void setViajes(DoubleLinkedList<VOViaje> viajes) {
		this.viajes = viajes;
	}

	@Override
	public int compareTo(VOViajesRuta4C o) {
		// TODO Auto-generated method stub
		return idRuta.compareTo(o.getIdRuta());
	}
	
	
	
	
	

}
