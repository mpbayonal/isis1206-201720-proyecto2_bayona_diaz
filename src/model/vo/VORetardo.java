package model.vo;

/**
 * Clase que modela un retardo aplicado a una parada del viaje
 */
public class VORetardo implements Comparable<VORetardo> {
	
	public VORetardo(int viajeid, int stopid, int tiempoRetardo) {
		super();
		this.viajeid = viajeid;
		this.stopid = stopid;
		this.tiempoRetardo = tiempoRetardo;
	}

	private int viajeid;
	private int stopid;
	
	/**
	 * Tiempo del retardo en segundos
	 */
	private int tiempoRetardo;

	public int getViajeid() {
		return viajeid;
	}

	public void setViajeid(int viajeid) {
		this.viajeid = viajeid;
	}

	public int getStopid() {
		return stopid;
	}

	public void setStopid(int stopid) {
		this.stopid = stopid;
	}

	public int getTiempoRetardo() {
		return tiempoRetardo;
	}

	public void setTiempoRetardo(int tiempoRetardo) {
		this.tiempoRetardo = tiempoRetardo;
	}

	@Override
	public int compareTo(VORetardo o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
