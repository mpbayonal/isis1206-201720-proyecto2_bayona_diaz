package model.vo;

public class VOShape implements Comparable<VOShape>
{

	private int id;
	private double latitude;
	private double longitude;
	private int point_sequence;
	private double dist_traveled;
	
	
	public VOShape(int id,  double latitude, double longitude, int point_sequence, double dist_traveled) {
		
		this.id = id;
		
		this.latitude = latitude;
		this.longitude = longitude;
		this.point_sequence = point_sequence;
		this.dist_traveled = dist_traveled;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getPoint_sequence() {
		return point_sequence;
	}
	public void setPoint_sequence(int point_sequence) {
		this.point_sequence = point_sequence;
	}
	public double getDist_traveled() {
		return dist_traveled;
	}
	public void setDist_traveled(int dist_traveled) {
		this.dist_traveled = dist_traveled;
	}
	
	@Override
	public int compareTo(VOShape o) 
	{
		if(id < o.getId()) {
			return -1;}
		else if(id > o.getId()) {
			return 1;}
		else {
		return 0;}
	}

}
