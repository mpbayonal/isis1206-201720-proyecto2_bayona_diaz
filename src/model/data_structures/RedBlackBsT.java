package model.data_structures;

import java.util.Iterator;

public class RedBlackBsT<K extends Comparable<K>, V> 
{
	private static boolean ROJO = true;
	private static boolean NEGRO = false;

	public RedBlackNode<K, V> raiz;


	public RedBlackBsT() {


		raiz = null;
	}

	public boolean isEmpty () 
	{
		return raiz == null;
	}

	public int size() 
	{
		if(raiz != null)
		return raiz.getSize();
		else
			return -1;
	}
	public V get(K llave) throws Exception 
	{
		V valorDevolver;
		if(llave != null) 
		{
			valorDevolver = get(llave, raiz);
		}
		else throw new Exception();
		return valorDevolver;
	}

	public V get(K llave , RedBlackNode<K, V> nodo) 
	{
		V valorDevolver = null;
		while(nodo != null) 
		{

			int comparador = llave.compareTo(nodo.getLlave());

			if(comparador > 0) 
			{
				nodo = nodo.getIzquierdo();
			}
			else if (comparador < 0) 
			{

				nodo = nodo.getDerecho();
			}
			else {
				return nodo.getValor();

			}


		}
		return valorDevolver;
	}

	public boolean contains(K llave) throws Exception 
	{

		return get(llave) != null;
	}

	public void put(K llave , V valor) throws Exception 
	{
		if(llave == null) 
		{
			throw new Exception("La llave es nula");
		}
		else if(valor == null) 
		{
			delete(llave);

		}
		else
		{
			raiz = put(raiz, llave, valor);
			raiz.setColor(NEGRO);

		}

	}

	public RedBlackNode<K, V> put(RedBlackNode<K, V>  nodo  ,K llave , V valor) throws Exception 
	{
		if(nodo == null ) 
		{

			RedBlackNode<K, V> nodoNuevo = new RedBlackNode<K, V>(llave, valor, 1, ROJO);

			return nodoNuevo;
		}
		else 
		{
			int comp = llave.compareTo(nodo.getLlave());
			if(comp < 0) 
			{
				nodo.setIzquierdo(put(  nodo.getIzquierdo()  , llave , valor)); 
			}
			else if(comp > 0) 
			{
				nodo.setDerecho(put(nodo.getDerecho(), llave, valor));
			}
			else
			{
				nodo.setValor(valor);
			}

			if(nodo.getDerecho().getColor() == ROJO && nodo.getIzquierdo().getColor() != ROJO) 
			{
				nodo = rotarIzquierda(nodo);
			}
			if (nodo.getIzquierdo().getColor() == ROJO && nodo.getIzquierdo().getIzquierdo().getColor() == ROJO) 
			{
				nodo = rotarDerecha(nodo);
			}
			if (nodo.getIzquierdo().getColor() == ROJO && nodo.getDerecho().getColor() == ROJO) 
			{
				nodo = cambiarColores(nodo);
			}
			nodo.setSize(nodo.getIzquierdo().getSize() + nodo.getDerecho().getSize() + 1);

			return nodo;
		}


	}



	public int height() 
	{
		return height(raiz);
	}

	public int height(RedBlackNode<K, V> nodo) 
	{
		if (nodo == null) 
		{
			return -1;
		}
		else 
		{
			int izq = height(nodo.getIzquierdo());
			int der = height(nodo.getDerecho());
			int altura = 0; 
			if(izq <= der) 
			{
				altura = der +1 ;
			}
			else 
			{
				altura = izq +1 ;
			}


			return altura ;
		}
	}

	public K min() throws Exception 
	{
		if(raiz == null) 
		{
			throw new Exception("El arbol esta vacio");
		}
		return min(raiz).getLlave();

	}
	
	public RedBlackNode<K, V> min(RedBlackNode<K, V> nodo) 
	{

		if(nodo.getIzquierdo() == null) 
		{
			return nodo;
		}
		else 
		{
			return min(nodo.getIzquierdo());
		}
		
		
		
	}

	public K max() throws Exception 
	{
		if(raiz == null) 
		{
			throw new Exception("El arbol esta vacio");
		}
		return max(raiz).getLlave();

		
	}
	
	public RedBlackNode<K, V> max(RedBlackNode<K, V> nodo) 
	{
		if(nodo.getDerecho() == null) 
		{
			return nodo;
		}
		else 
		{
			return max(nodo.getDerecho());
		}
	}

	public boolean check() 
	{
		return check(raiz, null, null);
	}
	
	public boolean check(RedBlackNode<K, V> nodo , K llaveMenor , K llaveMayor) 
	{
		if(nodo == null) 
		{
			return true;
		}
		if (llaveMenor != null && nodo.getLlave().compareTo(llaveMayor) <= 0) 
		{ 
			return false;
		}
        if (llaveMayor != null && nodo.getLlave().compareTo(llaveMayor) >= 0) 
        {
        	return false;
        }
        else 
        {
        return check(nodo.getIzquierdo(), llaveMenor, nodo.getLlave()) && check(nodo.getDerecho(), nodo.getLlave(), llaveMayor);
        }

	}

	public Iterator <K> keys() throws Exception
	{
		return keysInRange(min(), max());
	}

	public Iterator<V>valuesInRange(K init, K end) throws Exception
	{
		
		if(init == null || end == null) 
		{
			throw new Exception("El rango no es valido");
		}
		
		QueueIterable<V> cola = new QueueIterable<V>();
		
		introducirValoresCola(cola, init, end, raiz);
		
		return  cola.iterator();
	}
	
	private void introducirValoresCola(QueueIterable<V> cola , K inicio , K fin , RedBlackNode<K, V> nodo) 
	{
		if(nodo != null) 
		{
			int compInicio = inicio.compareTo(nodo.getLlave());
			int compFin = fin.compareTo(nodo.getLlave());
			
			if(compInicio < 0) 
			{
				introducirValoresCola(cola, inicio, fin, nodo.getIzquierdo());
			}
			if(compInicio <= 0 && compFin >= 0) 
			{
				cola.enqueue(nodo.getValor());
			}
			if(compFin > 0 ) 
			{
				introducirValoresCola(cola, inicio, fin, nodo.getDerecho());
			}
		
		}
	}
	
	public Iterator<K> keysInRange(K init, K end) throws Exception
	{

		if(init == null|| end == null) 
		{
			throw new Exception("El rango no es valido");
		}
		
		QueueIterable<K> cola = new QueueIterable<K>();
		
		introducirLlavesCola(cola, init, end, raiz);
		
		return  cola.iterator();
	}
	
	private void introducirLlavesCola(QueueIterable<K> cola , K inicio , K fin , RedBlackNode<K, V> nodo) 
	{
		if(nodo != null) 
		{
			int compInicio = inicio.compareTo(nodo.getLlave());
			int compFin = fin.compareTo(nodo.getLlave());
			
			if(compInicio < 0) 
			{
				introducirLlavesCola(cola, inicio, fin, nodo.getIzquierdo());
			}
			if(compInicio <= 0 && compFin >= 0) 
			{
				cola.enqueue(nodo.getLlave());
			}
			if(compFin > 0 ) 
			{
				introducirLlavesCola(cola, inicio, fin, nodo.getDerecho());
			}
		
		}
	}



	private RedBlackNode<K, V> rotarDerecha( RedBlackNode<K, V> nodo) 
	{
		RedBlackNode<K, V> temp = nodo.getIzquierdo();
		nodo.setIzquierdo(temp.getDerecho());
		temp.setDerecho(nodo);
		temp.setColor(temp.getDerecho().getColor());
		temp.getDerecho().setColor(ROJO);
		temp.setSize(nodo.getSize());
		nodo.setSize(1 + nodo.getDerecho().getSize() + nodo.getIzquierdo().getSize());
		return temp;
		

	}

	private RedBlackNode<K, V> rotarIzquierda(RedBlackNode<K, V> nodo) 
	{
		RedBlackNode<K, V> temp = nodo.getDerecho();
		nodo.setDerecho(temp.getIzquierdo());
		temp.setIzquierdo(nodo);
		temp.setColor(temp.getIzquierdo().getColor());
		temp.getIzquierdo().setColor(ROJO);
		temp.setSize(nodo.getSize());
		nodo.setSize(1 + nodo.getDerecho().getSize() + nodo.getIzquierdo().getSize());
		return temp;
		

	}

	private RedBlackNode<K, V> cambiarColores(RedBlackNode<K, V> nodo) 
	{
		nodo.setColor(!nodo.getColor());
		nodo.getIzquierdo().setColor(!nodo.getIzquierdo().getColor());
		nodo.getDerecho().setColor(!nodo.getDerecho().getColor());
		return nodo;

	}

//	public void deleteMin() {	}
//
//	public RedBlackNode<K, V> deleteMin(RedBlackNode<K, V> nodo) {}
//
//	public void deleteMax() {}

//	public RedBlackNode<K, V> deleteMax(RedBlackNode<K, V> nodo) {}

	public void delete(K llave) throws Exception 
	{
		if(llave == null) 
		{
			throw new Exception("La llave es nula");
		}
		if(!contains(llave)  ) 
		{
			
		}
		else 
		{
			if(raiz.getIzquierdo().getColor() == NEGRO && raiz.getDerecho().getColor() == NEGRO )
			{
				raiz.setColor(ROJO);
				raiz = delete(raiz , llave);
			}
			if(!isEmpty()) 
			{
				raiz.setColor(NEGRO);
			}
		}

	}

	public RedBlackNode<K, V> delete(RedBlackNode<K, V> nodo , K  llave) 
	{

		if(nodo.getIzquierdo().getColor() ==  ROJO) 
		{
			nodo = rotarDerecha(nodo);
		}
		if(llave.compareTo(nodo.getLlave()) == 0 && nodo.getDerecho().getIzquierdo().getColor() ==  ROJO ) 
		{
			
		}
		return nodo;
		
		
	}
	
	
}
