package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.PriorityQueue;

import javax.tools.JavaFileManager.Location;

import API.ISTSManager;
import model.vo.VOAgency;
import model.vo.VOBusUpdate;
import model.vo.VOEspecialService;
import model.vo.VOHoraParadaPorParada;
import model.vo.VOHoraParadaPorViaje;
import model.vo.VOInfo;
import model.vo.VOParada;
import model.vo.VOParadaNumeroIncidentes;
import model.vo.VOParadaTiempo;
import model.vo.VOParadasViaje;
import model.vo.VOPlan.ParadaPlanVO;
import model.vo.VOPlan.ParadaPlanVO.RutaPlanVO.ViajePlanVO;
import model.vo.VORangoHora;
import model.vo.VORetardo;
import model.vo.VORetardoViaje;
import model.vo.VORoute;
import model.vo.VORouteEstimate;
import model.vo.VOService;
import model.vo.VOServicio;
import model.vo.VOShape;
import model.vo.VOStop;
import model.vo.VOTransbordo;
import model.vo.VOTransfer;
import model.vo.VOTrip;
import model.vo.VOViaje;
import model.vo.VOViajesRuta4C;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedListSymbolTable;
import model.data_structures.IList;
import model.data_structures.IRedBlackBST;
import model.data_structures.LPHashTable;
import model.data_structures.PriorityQueueMax;
import model.data_structures.RedBlackBsT;
import model.data_structures.SeparateChainingHashTable;


public class STSManager implements ISTSManager {

	private LPHashTable<Integer, VOTrip> trips;
	private SeparateChainingHashTable<Integer, VOStop> stops;
	private DoubleLinkedListSymbolTable<Integer, VOTransfer> transfers;

	private LPHashTable<Integer, VORoute> routes;


	private DoubleLinkedList<VOAgency> agencies;
	private DoubleLinkedList<VOEspecialService> especialServices;
	private DoubleLinkedList<VOService> services;

	private DoubleLinkedList<VOShape> shapes;
	private DoubleLinkedList<VOInfo> feed_info;




	private DoubleLinkedList<VOBusUpdate> updates;
	private DoubleLinkedList<VORouteEstimate> estimate;
	private PersistenceManager persistencia;

	@Override
	public void ITSInit() 
	{
		stops = new SeparateChainingHashTable<Integer, VOStop>();
		trips = new LPHashTable<Integer, VOTrip>();
		transfers = new DoubleLinkedListSymbolTable<Integer, VOTransfer>();

		routes = new LPHashTable<Integer, VORoute>();


		agencies = new DoubleLinkedList<VOAgency>();
		especialServices = new DoubleLinkedList<VOEspecialService>();
		feed_info = new DoubleLinkedList<VOInfo>();
		services = new DoubleLinkedList<VOService>();
		shapes = new DoubleLinkedList<VOShape>();

		persistencia = new PersistenceManager();


	}
	
	@Override
	public void ITScargarGTFS1C() throws FileNotFoundException {
		// TODO Auto-generated method stub



		trips = persistencia.loadTrips("./data/trips.txt");
		stops = persistencia.loadStops("./data/stops.txt");


		routes = persistencia.loadRoutes("./data/routes.txt");
		transfers = persistencia.loadTransfers("./data/transfers.txt");


		loadStopTimes("./data/stop_times.txt");
		
		ITScargarTR("555");


		//	agencies = persistencia.loadAgencies("./data/agency.txt");



		//services = persistencia.loadServices("./data/calendar.txt");






		System.out.println("Se completo la carga");

	}

	
	public void ITScargarTR(String fecha) throws FileNotFoundException 
	{
		String day = fecha.substring(fecha.length()-2, fecha.length());
		String month = fecha.substring(fecha.length()-3, fecha.length()-2);
		String fecha1 = month +"-"+ day;
		String file = "./data/RealTime-"+month+"-"+day+"-BUSES_SERVICE";

		updates = persistencia.readBusUpdate("./data/BUSES_SERVICE");

		System.out.println("Se completo la carga");
	}

	public void loadStopTimes(String stopTimesFile) {


		String cadena;

		FileReader file = null;
		BufferedReader reader = null;
		String datos[];

		try
		{
			file= new FileReader(stopTimesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int tripId = Integer.parseInt(datos[0]);
					String arrivalTime = persistencia.getStringFromCsvLine(datos, 1);
					String departureTime = persistencia.getStringFromCsvLine(datos, 2);
					int stopId = persistencia.getIntFromCsvLine(datos, 3);


					VOHoraParadaPorParada viaje = new VOHoraParadaPorParada(tripId, arrivalTime, departureTime);
					VOHoraParadaPorViaje parada = new VOHoraParadaPorViaje(arrivalTime, departureTime, stopId);
					viaje.setRouteId(trips.get(tripId).getRouteId());
				
					trips.get(tripId).getTiemposViaje().put(parada.hashCode(), parada);
					stops.get(stopId).getTiemposViaje().put(viaje.hashCode(), viaje);

				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

	}





	/* 
	 * 1A
		-
		Dada una ruta y una fecha, identificar todos los viajes en los que hubo un retardo en al 
		menos  una  parada.  Se  debe  retornar  una  lista  ordenada  por  el  id  del  viaje 
		(descendentemente).  En  cada  posición  de  la  lista  se  encuentra  una  lista  de  paradas 
		ordenada de mayor a menor en tiempo de retardo.
		Nota1: 
		Las rutas deben estar almacenadas en una tabla de hash, utilizando Linear Probing como mecanismo de solución de colisiones.
		Nota2:
		¡Esta consulta se puede hacer de una forma eficiente! 
	 */
	
	/**
	 * Retorna una IList de viajes (ordenada por el id), que tuvieron retardo en almenos una parada. 
	 * @param idRuta
	 * @param fecha
	 * @return IList de VOViaje.
	 * @throws Exception 
	 */
	
	public IList<VOViaje> ITSviajesHuboRetardoParadas1A(String idRuta, String fecha) throws Exception 
	{
	
		//ITScargarTR(fecha);
		DoubleLinkedList<VOViaje> lista = new DoubleLinkedList<VOViaje>();
		



		Iterator<VOBusUpdate> iterador = updates.iterator();
		while(iterador.hasNext()) 
		{ 	
			VOBusUpdate temp = iterador.next();


			if (temp.getRouteNo().equals(idRuta) ) 
			{
				VOStop stop;
				try {
					stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());



					VOHoraParadaPorParada tiempo = stop.getTiemposViaje().get(temp.getTripId());

					if(tiempo!= null) {
						Date estimateDate = parsingDate(tiempo.getDepartureTime());
						Date realDate = parsingDate(temp.getRecordedTime());

						int diference = (int) ( (estimateDate.getTime() + 60000) - realDate.getTime());
						if(diference > 0) 
						{
							VOParadaTiempo paradaTiempo = new VOParadaTiempo(stop.getStopId(),diference );
							VOViaje temp1 = new VOViaje(temp.getTripId());
							
							if(!lista.existElement(temp1)) 
							{

								VOViaje viaje = new VOViaje(temp.getTripId());
								viaje.getParadas().add(paradaTiempo);
								lista.addInOrder(viaje);
								

							}
							else 
							{
								lista.findElement(temp1).getParadas().addInOrder(paradaTiempo);
							
							}
						}
					}
				} catch (Exception e) {
					if(!e.getMessage().equals("la parada no existe")) 
					{
						throw new Exception(e.getMessage());
					}
				}

			}

		}

		return lista;
	}
	/*
		2A - Identificar las n paradas en las que hubo más retardos en una fecha dada, teniendo en 
		cuenta todas las rutas que utilizan dicha parada.  Se debe retornar una lista con dichas 
		paradas, ordenada de mayor a menor por número total de retardos.
		En este punto debe utilizar un heap que almacena todas las paradas. Para cada parada, su
		número total de retardos debe ser utilizado como la prioridad
	 * */
	
	/**
	 * Retorna una lista de las n paradas que tuvieron m�s retardos en la fecha que llega por par�metro.
	 * @param fecha.
	 * @param n.
	 * @return Lista de VOParadas.
	 * @throws Exception 
	 */

	@Override
	public IList<VOParada> ITSNParadasMasRetardos2A(String fecha, int n) throws Exception {
		DoubleLinkedList<VOParada> lista = new DoubleLinkedList<VOParada>();
		PriorityQueueMax<Integer , VOParada> cola = ITSparadasRetrasadasFecha();
		
		for (int i = 0; i < n && !cola.isEmpty(); i++) 
		{
			lista.add(cola.dequeueElementoMayor());
			
			
		}
		return lista;
		
		
	}
	
	
	
	public PriorityQueueMax<Integer,VOParada> ITSparadasRetrasadasFecha() throws Exception 
	{
		
		
		PriorityQueueMax<Integer,VOParada> cola = new PriorityQueueMax<Integer,VOParada>();
		SeparateChainingHashTable<Integer, VOParada> listaRetrasos = new SeparateChainingHashTable<Integer, VOParada>();

		Iterator<VOBusUpdate> iterador = updates.iterator();
		while(iterador.hasNext()) 
		{ 		
			VOBusUpdate temp = iterador.next();

			VOStop stop;
			try {
				stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());
				VOHoraParadaPorParada tiempo = stop.getTiemposViaje().get(temp.getTripId());

				if(tiempo!= null) {

					Date estimateDate = parsingDate(tiempo.getDepartureTime());
					Date realDate = parsingDate(temp.getRecordedTime());

					int diference = (int) ( (estimateDate.getTime() + 60000) - realDate.getTime());
					if(diference > 0) 
					{
						VOViaje viaje = new VOViaje(temp.getTripId());

						if(!listaRetrasos.contiene(stop.getStopId())) 
						{
							
							VOParada parada = new VOParada();
							parada.setStopId(stop.getStopId());
							parada.setNumeroIncidentes(1);
							
							viaje.setTiempoRetraso(diference);
							parada.getViajes().add(viaje);

							listaRetrasos.put(parada.getStopId(), parada);

						}
						else 
						{
							
							listaRetrasos.get(stop.getStopId()).aumentarIncidentes();
							listaRetrasos.get(stop.getStopId()).getViajes().add(viaje);
						}
					}
				}
			} catch (Exception e) {
				if(!e.getMessage().equals("la parada no existe")) 
				{
					throw new Exception(e.getMessage());
				}
			}	


		}


		Iterator<VOParada> iterador1 =  listaRetrasos.iterador();

		while(iterador1.hasNext()) 
		{
			VOParada temp = iterador1.next();
			cola.enqueue(temp.getNumeroIncidentes(), temp);
			
		//	cola.enqueue(iterador1.next());

		}

		return cola;
	}

	/* 3A- Para una ruta dada en una fecha dada, retorne una lista ordenada (por tiempo total 
		de viaje) de todos los transbordos posibles, a otras rutas, a partir de las paradas de dicha 
		ruta (las paradas de una ruta son las paradas de todos sus viajes). Es posible hacer un 
		trasbordo  en  una  parada,  si  esa  parada  sirve  a  diferentes  rutas.  Un  transbordo  se  da 
		cuando se llega a una parada de una ruta y en la misma parada se puede tomar una ruta 
		diferente. Una vez finalizado el análisis de cada transbordo, retorne a la parada donde 
		inició el trasbordo y continúe el mismo proceso de transbordos en las paradas que siguen 
		hasta terminar la ruta de consulta.

		Nota:  Al  hacer  el  análisis  de  posibles  transbordos hay  que  evitar  los  ciclos  infinitos  al 
		volver  a  una  parada  que  ya  fue  analizada.  En  el  análisis  de  los  transbordos  desde  una 
		parada,  se  sugiere  colocar  una  marca  a  cada  parada  por  la  que  se  vaya  pasando  y  así 
		reconocer si una parada ya fue analizada.
		Proponer una solución que mejore el tiempo del proyecto 1 usando nuevas estructuras de datos (
		heap, tabla hash y arboles rojo - negro). Si no hay una mejor solución mostrar un 
		análisis experimental de tiempos de ejecución revisando otra opción de solución. Reporte los tiempos del proyecto 1 y    
		el proyecto 2 en una tabla para diferentes rutas (adjuntar la tabla en un documento de tiempos de ejecución). 
	 * 
	 * */
	
	
	@Override
	public IList<VOTransbordo> ITStransbordosRuta3A(String idRuta, String fecha) throws Exception 
	{
	//	ITScargarTR(fecha);

		DoubleLinkedList<VOTransbordo> lista = new DoubleLinkedList<VOTransbordo>();

		Iterator<VOBusUpdate> iterador = updates.iterator();
		while(iterador.hasNext()) 
		{ 
			VOBusUpdate temp = iterador.next();
			if (temp.getRouteNo().equals(idRuta) ) 
			{

				VOStop stop;
				try {
					stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());
					
					VOTransbordo nuevo2 = new VOTransbordo(parsingDate(temp.getRecordedTime()),stop.getStopId());
					
					VOTransbordo nuevo = lista.findElement(nuevo2);
					
					if(nuevo == null) 
					{
						Iterator<VOHoraParadaPorParada> ite = stop.getTiemposViaje().iterador();
						while(ite.hasNext()) 
						{ VOHoraParadaPorParada temp1 = ite.next();
	
						VOTrip viajeActual = trips.get(temp1.getTripId());
						VORoute rutaActual = routes.get(viajeActual.getRouteId());
	
						if(rutaActual != null)
						{
							if(!idRuta.equals(rutaActual.getRouteShortName())) 
							{
								
								VOViajesRuta4C rutaActualAñadir = new VOViajesRuta4C(rutaActual.getRouteId(), stop.getStopId());
								VOViaje viaje = new VOViaje(temp1.getTripId());
								if(nuevo2.getListadeParadas().existElement(rutaActualAñadir)) 
								{
									nuevo2.getListadeParadas().findElement(rutaActualAñadir).getViajes().add(viaje);
								}
								else 
								{
									rutaActualAñadir.getViajes().add(viaje);
									nuevo2.getListadeParadas().add(rutaActualAñadir);
								}
							
								lista.add(nuevo2);
		
							}
							
							
						}
						
						
					
					}
					
					}

				} catch (Exception e) {
					if(!e.getMessage().equals("la parada no existe")) 
					{
						throw new Exception(e.getMessage());
					}
				}





			}


		}

		return lista;
	}

	


	//	/*2C- Identificar	los	n viajes	que	recorren	más	distancia	en	una	fecha	dada, retornando	una	
	//lista	ordenada	por	esta	distancia.	Utilice	un	heap	para	almacenar	todos	los	viajes.*/

	@Override
	public IList<VOServicio> ITSNViajesMasDistancia2C(int n, String fecha) throws Exception {
		DoubleLinkedList<VOServicio> listaServicios = new DoubleLinkedList<VOServicio>();
		PriorityQueueMax<Double , VOServicio> cola = ITSserviciosMayorDistancia(fecha);
		
		for (int i = 0; i < n && !cola.isEmpty(); i++) 
		{
			
			listaServicios.add(cola.dequeueElementoMayor());
		}
		return listaServicios;
	}
	
	
	
		public PriorityQueueMax<Double,VOServicio> ITSserviciosMayorDistancia(String fecha) throws Exception
		{
			//ITScargarTR(fecha);
			
			PriorityQueueMax<Double, VOServicio> cola = new PriorityQueueMax<Double, VOServicio>();
			
			DoubleLinkedList<VOServicio> listaServicios = new DoubleLinkedList<VOServicio>();
			Iterator<VOBusUpdate> listaIterador = updates.iterator();
			while(listaIterador.hasNext()) 
			{
				VOBusUpdate update = listaIterador.next();
				VOServicio servicio = new VOServicio(update.getTripId(), 0.0);
	
				if(!listaServicios.existElement(servicio)) 
				{
					Iterator<VOBusUpdate> listaIterador2 = updates.iterator();
					double distanciaLON = 0;
					double distanciaLAT = 0;
					double distanciaFinal = 0;
					while(listaIterador2.hasNext()) 
					{
						VOBusUpdate update2 = listaIterador2.next();
						if(update2.getTripId() == servicio.getServiceId())
							if(servicio.getDistanciaRecorrida() == 0 && distanciaLON == 0) 
							{
								distanciaLON = update2.getLongitude();
								distanciaLAT = update2.getLatitude();
							}
							else 
							{
								double distancia = getDistance(distanciaLAT, distanciaLON, update2.getLatitude(), update2.getLongitude());
								distanciaLON = update2.getLongitude();
								distanciaLAT = update2.getLatitude();
								distanciaFinal += distancia;
							}
	
					}
					servicio.setDistanciaRecorrida(distanciaFinal);
					
					listaServicios.add(servicio);
					
				}
				
			}
	

			Iterator<VOServicio> iterador = listaServicios.iterator();
			while(iterador.hasNext()) 
			{
				VOServicio temp = iterador.next();
				cola.enqueue(temp.getDistanciaRecorrida(), temp);
			
			}
			
	
			return cola;
		}
	
	
		//	/*3C- Retornar	un	árbol	balanceado	de	retardos	de	un	viaje	 (dado	su	identificador) en	una	
		//fecha	dada.	El	árbol	debe	estar	organizado	por	tiempo	de	retardo	en	sus	paradas.	Utilice	
		//el	cálculo	de	paradas	para	un	viaje	utilizado	en	el	 taller	3	y	la	estimación	de	 tiempos	de	
		//retardo	a	partir	de	los	datos	 tiempo	 real	de	los	viajes	en	la	 fecha	dada.	Si	hay	múltiples	
		//paradas	con	el	mismo	tiempo	de	retardo	(llave),	el	nodo	debe	guardar	todas	las	paradas.*/
	
	@Override
	public IRedBlackBST<Integer, IList<VORetardo>> retardosViajeFecha3C(String idViaje, String fecha) throws Exception 
	{
		RedBlackBsT<Integer, DoubleLinkedList<VORetardo>> arbol = new RedBlackBsT<Integer,DoubleLinkedList<VORetardo>>();

		Iterator<VOBusUpdate> iterador = updates.iterator();
		while(iterador.hasNext()) 
		{ 		
			VOBusUpdate temp = iterador.next();

			VOStop stop;
			if(temp.getTripId() == Integer.parseInt(idViaje)) {
				try {
					stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());
					VOHoraParadaPorParada tiempo = stop.getTiemposViaje().get(temp.getTripId());
	
					if(tiempo!= null) {
	
						Date estimateDate = parsingDate(tiempo.getDepartureTime());
						Date realDate = parsingDate(temp.getRecordedTime());
	
						int diference = (int) ( (estimateDate.getTime() + 60000) - realDate.getTime());
						if(diference > 0) 
						{
							VORetardo viaje = new VORetardo(Integer.parseInt(idViaje), stop.getStopId() , diference);
	
							DoubleLinkedList<VORetardo> lista = arbol.get(diference);
							if(lista != null) 
							{
								lista.add(viaje);
							}
							else 
							{
								lista = new DoubleLinkedList<VORetardo>();
								lista.add(viaje);
								arbol.put(diference, lista);
							}
						}
					}
				} catch (Exception e) {
					if(!e.getMessage().equals("la parada no existe")) 
					{
						throw new Exception(e.getMessage());
					}
				}	


		}
			}
		
		return (IRedBlackBST<Integer, IList<VORetardo>>) arbol;
	}


	
	
	
	//	/**4C- Dada	una	parada	y	una	fecha,	responder	si	dicha	parada	es	compartida	o	no	en	dicha	
	//fecha.	Una	parada	es	compartida	si	hay	rutas	diferentes	que	usan	la	misma	parada	en	la	
	//planeación	de	sus	viajes.	Si	la	parada	es	compartida	se	debe	retornar	la	información	de	las	
	//rutas	 y	 sus	 viajes	 de	 dicha	 parada. Para	 resolver	 esta	 consulta,	 construya	 árbol	 binario	
	//ordenado	balanceado,	con	todas	las	paradas	compartidas	en	una	 fecha	dada.	Cada	nodo	
	//del	 árbol,	 contiene	 una	 parada	 compartida	 y	 una	 lista	 de	 las	 rutas	 que	 paran	 en	 dicha	
	//parada;	cada	ruta	tiene	una	lista	de	todos	sus	viajes	que	paran	en	la	parada.	*/
	//	
	
	@Override
	public IList<VOViajesRuta4C> ITSParadasCompartidas4C(String idStop) throws NumberFormatException, Exception {
		RedBlackBsT<Integer,  DoubleLinkedList<VOViajesRuta4C>> arbol = paradasCompartidas();
		return arbol.get(Integer.parseInt(idStop));
		
	}
	
	public RedBlackBsT<Integer,  DoubleLinkedList<VOViajesRuta4C>> paradasCompartidas() throws Exception 
	{
		RedBlackBsT<Integer, DoubleLinkedList<VOViajesRuta4C>> arbol = new RedBlackBsT<Integer,DoubleLinkedList<VOViajesRuta4C>>();

		Iterator<VOBusUpdate> iterador = updates.iterator();
		while(iterador.hasNext()) 
		{ 		
			VOBusUpdate temp = iterador.next();

			VOStop stop;
			
				try {
					stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());
					
					DoubleLinkedList<VOViajesRuta4C> lista = arbol.get(stop.getStopId());
					if(lista == null) 
					{
						LPHashTable<Integer, VOHoraParadaPorParada> tiempos = stop.getTiemposViaje();
						lista = new DoubleLinkedList<VOViajesRuta4C>();
						Iterator<VOHoraParadaPorParada> ite = tiempos.iterador();
						while(ite.hasNext()) 
						{
							VOHoraParadaPorParada actual = ite.next();
							
							VOViajesRuta4C viaje = new VOViajesRuta4C(actual.getRouteId(), stop.getStopId());
							VOViaje viajeAnadir = new VOViaje(actual.getTripId());
							viaje.getViajes().add(viajeAnadir);
							VOViajesRuta4C viajeRuta = lista.findElement(viaje);
							if(viajeRuta == null) 
							{
								lista.add(viaje);
							}
							else 
							{
								viajeRuta.getViajes().add(viajeAnadir);
							}
							
							
							
						}
						
						arbol.put(stop.getStopId(), lista);
						
					}
				
	
					
					}
				 catch (Exception e) 
				{
					if(!e.getMessage().equals("la parada no existe")) 
					{
						throw new Exception(e.getMessage());
					}
				}	


		}
		return arbol;
			}
		
	
	
	
	@Override
	public IList<VOHoraParadaPorParada> ITSViajesPararonEnRango5C(String idRuta, String horaInicio, String horaFin) throws NumberFormatException, Exception {
		RedBlackBsT<Date, VOHoraParadaPorParada> arbol = darviajesParadaPorHora(Integer.parseInt(idRuta));
		DoubleLinkedList<VOHoraParadaPorParada> lista = new DoubleLinkedList<VOHoraParadaPorParada>();
		Date inicio = parsingDate(horaInicio);
		Date fin = parsingDate(horaFin);
		Iterator<VOHoraParadaPorParada> iterador = arbol.valuesInRange(inicio, fin);
		while(iterador.hasNext()) 
		{
			lista.add(iterador.next());
		}
		return lista;
	}
	

	
	
	
	//	/*5C- Dada	 una	 ruta,	 dar	 todos	 los	 viajes	 (con	 sus	 respectivas	 paradas)	 que	 realizaron	
	//paradas	(recuerde	el	cálculo	de	paradas	para	un	viaje	utilizado	en	el	taller	3)	en	un	rango	
	//de	tiempo	dado.	El	rango	de	tiempo	está	dado	como	hora	y	minuto	inicial,	y	hora	y	minuto	
	//final.	 Para	 resolver	 este	 problema	 deben	 construir	 un	 árbol	 balanceado	 que	 contiene	
	//todos	los	reportes	de	tiempo	real	que	coincidieron	con	paradas	de	viajes	de	una	ruta.**/
													
	public RedBlackBsT<Date, VOHoraParadaPorParada> darviajesParadaPorHora(int routeId) throws Exception
	{
		RedBlackBsT<Date, VOHoraParadaPorParada> arbol = new RedBlackBsT<Date,VOHoraParadaPorParada>();
		Iterator<VOStop> ite1 = stops.iterador();
		while(ite1.hasNext()) 
		{
			Iterator<VOHoraParadaPorParada> ite = ite1.next().getTiemposViaje().iterador();
			while(ite.hasNext()) 
			{
				VOHoraParadaPorParada actual = ite.next();
				Date horaActual = parsingDate(actual.getArrivalTime());
				arbol.put(horaActual, actual);
			}
			}
		
		return arbol;
	}
	

	
	
	//	@Override
	//	public IList<VORuta> ITSviajesParadasRangoDeTiemp(String ruta, String fecha, String horaInicio, String horaFin) {
	//		// TODO Auto-generated method stub
	//		return null;
	//	}
	//
	//
	//	public Stack<VOParadaNumeroIncidentes> listStops(Integer tripID) 
	//	{
	//		Stack<VOParadaNumeroIncidentes> pila = new Stack<VOParadaNumeroIncidentes>();
	//		Queue<VOBusUpdate> update = updates;
	//
	//		VOBusUpdate actual = null;
	//		while (!update.isEmpty())
	//		{
	//			actual = update.dequeue();
	//			if(actual.getTripId() == tripID)
	//			{
	//				Iterator<VOParadaNumeroIncidentes> iterator = stops.iterator();
	//				while(iterator.hasNext())
	//				{
	//					VOParadaNumeroIncidentes actualStop = iterator.next();
	//					Double distance = getDistance(actual.getLatitude(), actual.getLongitude(), actualStop.getStopLat(), actual.getLongitude());
	//
	//					if(distance <= 70)
	//					{
	//						pila.push(actualStop);
	//					}
	//				}
	//			}
	//		}
	//		return pila;
	//	}
	//
	//

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		//final int R = 6371*1000; // Radious of the earth


		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;

		dist = dist * 1.609344;


		return (dist);


		//		Double latDistance = toRad(lat2-lat1);
		//		Double lonDistance = toRad(lon2-lon1);
		//		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
		//				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
		//				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		//		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		//		Double distance = R * c;
		//
		//		return distance;


	}



	private Double toRad(Double value)
	{
		int f = 180;
		return value * Math.PI / f;

	}



	public VOStop stopByCoordinates(Double latitude, Double longitude) throws Exception 
	{
		VOStop stop= null;
		boolean find = false;
		Iterator<VOStop> iterator = stops.iterador();
		while(iterator.hasNext() && !find)
		{
			VOStop actualStop = iterator.next();
			Double distance = getDistance(latitude, longitude, actualStop.getStopLat(), actualStop.getStopLon());
			if(distance <= 70.00)
			{
				stop = actualStop;
				find = true;
			}
		}

		if(stop == null) 
		{
			throw new Exception("la parada no existe");
		}
		return stop;
	}



	public Date parsingDate(String date) throws ParseException 
	{	
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss a");
		DateFormat formatMilitar = new SimpleDateFormat( "HH:mm:ss");
		Date time = null;

		try 
		{
			time = format.parse(date);
		} catch (ParseException e) {

			time = formatMilitar.parse(date);	 
		}

		if (time != null) 
		{
			String formattedDate = formatMilitar.format(time);
		}

		return time;

	}

	public int getTimeDIference(Date fecha1, Date fecha2) 
	{
		long p = fecha2.getTime();

		return (int) (fecha1.getTime() -  (fecha2.getTime() + 60000));
	}



	/*
	1B- Dada una ruta y una fecha, identificar todos los viajes en los que después de un retardo, todas las paradas siguientes tuvieron 
	retardo. Se debe retornar una lista ordenada (descendentemente) por el id del viaje. En cada posición de la lista se encuentra una 
	lista de paradas ordenada de mayor a menor en tiempo de retardo.
	Nota1: Las rutas deben estar almacenadas en una tabla de hash, utilizando Linear Probing como mecanismo de solución de colisiones.
 */
	

	@Override
	public LPHashTable<Integer,VOViaje > ITSviajesRetrasoTotalRuta1B(String idRuta, String fecha) throws Exception {
		
		LPHashTable<Integer,VOViaje > r= new LPHashTable<>();
		Iterator<VOBusUpdate> iUpdates= updates.iterator();
		while(iUpdates.hasNext()) {
			VOBusUpdate update= iUpdates.next();
			
			if(update.getRouteNo().equals(idRuta)) {
				VOStop stop;
				try {
					stop = stopByCoordinates(update.getLatitude(), update.getLongitude());
					VOHoraParadaPorParada tiempo = stop.getTiemposViaje().get(update.getTripId());

					if(tiempo!= null) {
						Date estimateDate = parsingDate(tiempo.getDepartureTime());
						Date realDate = parsingDate(update.getRecordedTime());

						int diference = (int) ( (estimateDate.getTime() + 60000) - realDate.getTime());
						if(diference > 0) 
						{
							VOParadaTiempo paradaTiempo = new VOParadaTiempo(stop.getStopId(),diference );
							VOViaje temp1 = new VOViaje(update.getTripId());
							
							if(r.get(update.getTripId())==null) 
							{
								VOViaje viaje = new VOViaje(update.getTripId());
								viaje.getParadas().add(paradaTiempo);
								r.put(update.getTripId(), viaje);

							}
							else 
							{
								r.get(update.getTripId()).getParadas().addInOrder(paradaTiempo);
							
							}
						}
					}
				} catch (Exception e) {
					if(!e.getMessage().equals("la parada no existe")) 
					{
						throw new Exception(e.getMessage());
					}
				}
			}
		}
		
		
		return r;
	}
	/*
	 2B-Identificar la franja de hora entera (ejemplo: 2:00pm a 3:00 pm o 5:00 pm a 6:00 pm) en las que hubo
	  más paradas con retardos, para una ruta determinada en una fecha dada. Se debe retornar la hora entera
	   de más retardos de la ruta incluyendo la lista de todos los viajes con retardo durante esta hora.
	
	Nota: Almacene las rutas en una tabla de hash, utilizando Separate Chaining como mecanismo de solución de
   colisiones. Por cada ruta se debe tener una cola de prioridad indexada por franja de hora, donde la prioridad 
   corresponde a el número total de retardos.
	 */


	@Override
	public VORangoHora ITSretardoHoraRuta2B(String idRuta, String fecha) throws Exception {
		DoubleLinkedList<VORangoHora> r= new DoubleLinkedList<VORangoHora>();
		Iterator<VOBusUpdate> iUpdates= updates.iterator();
			while(iUpdates.hasNext()) {
				VOBusUpdate update= iUpdates.next();
				if(update.getRouteNo().equals(idRuta)) {
					VOStop stop;
					try {
						stop = stopByCoordinates(update.getLatitude(), update.getLongitude());
						VOHoraParadaPorParada tiempo = stop.getTiemposViaje().get(update.getTripId());

						if(tiempo!= null) {
							Date estimateDate = parsingDate(tiempo.getDepartureTime());
							Date realDate = parsingDate(update.getRecordedTime());

							int diference = (int) ( (estimateDate.getTime() + 60000) - realDate.getTime());
							if(diference > 0) 
							{
								int hora= estimateDate.getHours();
								VORetardoViaje retardoViaje= new VORetardoViaje(update.getTripId());
								retardoViaje.adicionarTiempo(diference);
								VORangoHora retardoHora= new VORangoHora(hora);
								VORangoHora temp=r.findElement(retardoHora);
								if(temp!=null) {
									temp.addRetardoViaje(retardoViaje);
								}
								else {
									retardoHora.addRetardoViaje(retardoViaje);
									r.add(retardoHora);
								}
							}
						}
					} catch (Exception e) {
						if(!e.getMessage().equals("la parada no existe")) 
						{
							throw new Exception(e.getMessage());
						}
					}
				}
			}
			VORangoHora mayor=null;
			double max=0;
			Iterator<VORangoHora> iRango= r.iterator();
				while(iRango.hasNext()) {
					VORangoHora m=iRango.next();
					if(m.totalRetardo()>max) {
						mayor=m;
						max=m.totalRetardo();
					}
				}
			return mayor;
	}

	
	/*
	3B- Dados	los	Ids	de	una	parada	de	origen	y	una	parada	destino	buscar	los	viajes	de	rutas	
//	de	bus	para	ir	del	origen	al	destino	(sin	hacer	trasbordos,	es	decir	llegando	al	destino	en	el	
//	mismo	viaje/bus	que	se	toma	en	el	origen)	en	una	fecha	y	franja	de	horario	(hora	entera	
//	inicial	y	hora	entera	 final)	dadas.	Por	ejemplo,	buscar	los	viajes	de	 rutas	entre	la	parada	
//	11940	y	la	parada	9224	en	la	franja	12:00	pm	– 2:00	pm. Para	los	viajes	de	rutas	que	sean	
//	solución	al	problema	se	debe	informar	el	Id	de	la	ruta,	el	Id	del	viaje,	el	tiempo	del	viaje	en	
//	la	parada	origen	y	el	tiempo	del	viaje	en	la	parada	destino.
//	Proponer	una	solución	que	mejore	el	tiempo	del	proyecto	1	usando	nuevas	estructuras	de	
//	datos	 (heap,	tabla	 hash y	arboles	 rojo-negro).	 Si	 no	 hay	 una	mejor	 solución	mostrar	 un	
//	análisis	experimental	de	tiempos	revisando	otra	opción	de	solución.	Reporte	los	tiempos	
//	del	proyecto	1	y	el	proyecto	2	en	una	tabla	para	diferentes	datos	de	entrada	(adjuntar	la	
//	tabla en	un	documento	de	tiempos	de	ejecución)
*/
	
	@Override
	public DoubleLinkedList<VOViaje> ITSbuscarViajesParadas3B(String idOrigen, String idDestino, String fecha,
			String horaInicio, String horaFin) throws NumberFormatException, Exception 
	{
		DoubleLinkedList<VOViaje> rTrips= new DoubleLinkedList<VOViaje>();
		Iterator<VOTrip> it= trips.iterador();
		while (it.hasNext()) {
			VOTrip trip = it.next();
			LPHashTable<Integer, VOHoraParadaPorViaje> r= trip.getTiemposViaje();
			 VOHoraParadaPorViaje origen=r.get(Integer.parseInt(idOrigen));
			if(origen!=null) {
				if(origen.getArrivalTime().equals(horaInicio)) {
					VOHoraParadaPorViaje destino=r.get(Integer.parseInt(idOrigen));
					if(destino!=null) {
						VOViaje viaje= new VOViaje(trip.getTripId());
						rTrips.add(viaje);
					}
					
				}

			}
		}
		
		
		
			
		

		
		return rTrips;
	}


	
}
	
