package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.vo.VOAgency;
import model.vo.VOBusUpdate;
import model.vo.VOEspecialService;
import model.vo.VOInfo;
import model.vo.VORoute;
import model.vo.VORouteEstimate;
import model.vo.VORuta;
import model.vo.VOService;
import model.vo.VOShape;
import model.vo.VOStop;
import model.vo.VOStopTime;
import model.vo.VOTransfer;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedListSymbolTable;
import model.data_structures.LPHashTable;
import model.data_structures.SeparateChainingHashTable;

public class PersistenceManager {




	public PersistenceManager() {


	}

	public String getStringFromCsvLine(String[] csvLineFields, int index) {
		return csvLineFields[index].trim();
	}

	public int getIntFromCsvLine(String[] csvLineFields, int index) {
		return Integer.parseInt(getStringFromCsvLine(csvLineFields, index));
	}

	public Integer getNullableIntFromCsvLine(String[] csvLineFields, int index) {
		int number = 0;
		try 
		{
			number = getStringFromCsvLine(csvLineFields, index).isEmpty() ? null : getIntFromCsvLine(csvLineFields, index);
		} catch (Exception e) {

		}
		return number;
	}


	public DoubleLinkedList<VOShape> loadShapes(String shapesFile) {

		DoubleLinkedList<VOShape> shape = new DoubleLinkedList<VOShape>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(shapesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");
					int id = getIntFromCsvLine(csvLineFields, 0);

					double latitude = Double.parseDouble(getStringFromCsvLine(csvLineFields, 1));
					double longitude = Double.parseDouble(getStringFromCsvLine(csvLineFields, 2));
					int point_sequence = getIntFromCsvLine(csvLineFields, 3);
					Double dist_traveled = Double.parseDouble(getStringFromCsvLine(csvLineFields, 4));

					VOShape newShape = new VOShape(id, latitude, longitude, point_sequence, dist_traveled);
					shape.addAtEnd(newShape);
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

		return shape;
	}

	public DoubleLinkedList<VOAgency> loadAgencies(String agencyFile) {

		DoubleLinkedList<VOAgency> agencies = new DoubleLinkedList<VOAgency>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(agencyFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");
					VOAgency newAgency = readAgencyFromCsvLine(csvLineFields);
					agencies.addAtEnd(newAgency); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

		return agencies;
	}


	private VOAgency readAgencyFromCsvLine(String[] csvLineFields) {
		String id = getStringFromCsvLine(csvLineFields, 0);
		String name = getStringFromCsvLine(csvLineFields, 1);
		String url = getStringFromCsvLine(csvLineFields, 2);
		String timezone = getStringFromCsvLine(csvLineFields, 3);
		String lang = getStringFromCsvLine(csvLineFields, 4);

		return new VOAgency(id, name, url, timezone, lang);
	}

	public DoubleLinkedList<VOService> loadServices(String calendarFile) {

		DoubleLinkedList<VOService> services = new DoubleLinkedList<VOService>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(calendarFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					int id = getIntFromCsvLine(csvLineFields, 0);
					int monday = getIntFromCsvLine(csvLineFields, 1);
					int tuesday = getIntFromCsvLine(csvLineFields, 2);
					int wednesday = getIntFromCsvLine(csvLineFields, 3);
					int thursday = getIntFromCsvLine(csvLineFields, 4);
					int friday = getIntFromCsvLine(csvLineFields, 5);
					int saturday = getIntFromCsvLine(csvLineFields, 6);
					int sunday = getIntFromCsvLine(csvLineFields, 7);
					int start_date = getIntFromCsvLine(csvLineFields, 8);
					int end_date = getIntFromCsvLine(csvLineFields, 9);

					VOService newService = new VOService(id, monday, tuesday, wednesday, thursday, friday, saturday, sunday, start_date, end_date);
					services.addAtEnd(newService); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

		return services;
	}

	public DoubleLinkedList<VOInfo> loadInfo(String feed_info) {

		DoubleLinkedList<VOInfo> info = new DoubleLinkedList<VOInfo>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(feed_info);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					String feed_publisher_name = getStringFromCsvLine(csvLineFields, 0);
					String feed_publisher_url = getStringFromCsvLine(csvLineFields, 1);
					String feed_lang = getStringFromCsvLine(csvLineFields, 2);
					int feed_start_date = getIntFromCsvLine(csvLineFields, 3);
					int feed_end_date = getIntFromCsvLine(csvLineFields, 4);
					String feed_version = getStringFromCsvLine(csvLineFields, 4);


					VOInfo newService = new VOInfo(feed_publisher_name, feed_publisher_url, feed_lang, feed_start_date, feed_end_date, feed_version);
					info.addAtEnd(newService); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

		return info;
	}


	public DoubleLinkedList<VOEspecialService> loadEspecialServices(String calendarDateFile) {

		DoubleLinkedList<VOEspecialService> services = new DoubleLinkedList<VOEspecialService>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(calendarDateFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					int id = getIntFromCsvLine(csvLineFields, 0);
					int date = getIntFromCsvLine(csvLineFields, 1);
					int exception_type = getIntFromCsvLine(csvLineFields, 2);

					VOEspecialService newService = new VOEspecialService(id, date, exception_type);
					services.addAtEnd(newService); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

		return services;
	}

	public DoubleLinkedListSymbolTable<Integer, VOTransfer> loadTransfers(String transfersFile) {

		 DoubleLinkedListSymbolTable<Integer, VOTransfer> transfers = new  DoubleLinkedListSymbolTable<Integer, VOTransfer>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(transfersFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					int from_stop_id = getIntFromCsvLine(csvLineFields, 0);
					int to_stop_id = getIntFromCsvLine(csvLineFields, 1);
					int transfer_type = getIntFromCsvLine(csvLineFields, 2);
					int min_transfer_time = getNullableIntFromCsvLine(csvLineFields, 3);

					VOTransfer newTransfer = new VOTransfer(from_stop_id, to_stop_id, transfer_type, min_transfer_time);
					transfers.add(newTransfer, newTransfer.hashCode()); 
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

		return transfers;
	}


	public DoubleLinkedList<VOBusUpdate> readBusUpdate(String rtFile ) throws FileNotFoundException 
	{
		
		DoubleLinkedList<VOBusUpdate> updates = new DoubleLinkedList<VOBusUpdate>();
		File f = new File(rtFile);
		File[] updateFiles = f.listFiles();
		for (int j = 0; j < updateFiles.length; j++) {
			if(updateFiles[j].getName().length() > 15) 
			{
			Gson gson = new Gson(); 
			BufferedReader reader = new BufferedReader(new FileReader(updateFiles[j]));
			gson = new GsonBuilder().create();
			VOBusUpdate[] update = gson.fromJson(reader, VOBusUpdate[].class);
			for (int i = 0; i < update.length; i++) 
			{
				updates.addAtEnd(update[i]);
			}
			}
		}
		return updates;




	}	

	public DoubleLinkedList<VORouteEstimate> readStopsEstimateService(String rtFile) throws FileNotFoundException 
	{
		DoubleLinkedList<VORouteEstimate> stopsEstimate = new DoubleLinkedList<VORouteEstimate>();
		File f = new File(rtFile);
		File[] updateFiles = f.listFiles();
		for (int j = 0; j < updateFiles.length; j++) {
			Gson gson = new Gson(); 
			BufferedReader reader = new BufferedReader(new FileReader(updateFiles[j]));
			gson = new GsonBuilder().create();
			VORouteEstimate[] update = gson.fromJson(reader, VORouteEstimate[].class);
			for (int i = 0; i < update.length; i++) 
			{
				stopsEstimate.addAtEnd(update[i]);
			}
		}
		return stopsEstimate;
	}

	public LPHashTable<Integer, VORoute> loadRoutes(String routesFile) {
		String cadena;

		LPHashTable<Integer, VORoute> routes = new LPHashTable<Integer, VORoute>();

		FileReader file = null;
		BufferedReader reader = null;

		try 
		{
			file= new FileReader(routesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					int routeId = getIntFromCsvLine(csvLineFields, 0);
					String agencyId = getStringFromCsvLine(csvLineFields, 1);
					String routeShortName = getStringFromCsvLine(csvLineFields, 2);
					String routeLongName = getStringFromCsvLine(csvLineFields, 3);
				

					VORoute newRoute = new VORoute(routeId, agencyId, routeShortName, routeLongName);


					routes.put(newRoute.hashCode(), newRoute); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		} 
		catch (Exception e) 
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		return routes;
	}


	public LPHashTable<Integer, VOTrip> loadTrips(String tripsFile) {

		LPHashTable<Integer, VOTrip> trips = new  LPHashTable<Integer, VOTrip>();
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(tripsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					int routeId = getIntFromCsvLine(csvLineFields, 0);
					int serviceId = getIntFromCsvLine(csvLineFields, 1);
					int tripId = getIntFromCsvLine(csvLineFields, 2);
					
					String tripShortName = getStringFromCsvLine(csvLineFields, 4);
					

					VOTrip newTrip = new VOTrip(routeId, serviceId, tripId, tripShortName);

					trips.put(newTrip.hashCode(), newTrip); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		return trips;
	}


	


	public SeparateChainingHashTable<Integer,VOStop> loadStops(String stopsFile) {
		String cadena;

		SeparateChainingHashTable<Integer,VOStop> stops = new SeparateChainingHashTable<Integer,VOStop>();

		FileReader file = null;
		BufferedReader reader = null;
		String datos[] = null;

		try
		{
			file= new FileReader(stopsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int stopId = getIntFromCsvLine(datos, 0);
					Integer stopCode = getNullableIntFromCsvLine(datos, 1);
					String stopName = getStringFromCsvLine(datos, 2);
					
					double stopLat = Double.parseDouble(getStringFromCsvLine(datos, 4));
					double stopLon = Double.parseDouble(getStringFromCsvLine(datos, 5));
					

					VOStop newStop = new VOStop(stopId, stopCode, stopName, stopLat, stopLon);


					stops.put(newStop.hashCode(), newStop);
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		return stops;
	}





}
