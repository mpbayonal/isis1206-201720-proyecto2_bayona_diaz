package model.logic;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.Date;
import java.util.PriorityQueue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.IRedBlackBST;
import model.data_structures.LPHashTable;
import model.data_structures.PriorityQueueMax;
import model.data_structures.RedBlackBsT;
import model.vo.VOHoraParadaPorParada;
import model.vo.VOParada;
import model.vo.VOParadaNumeroIncidentes;
import model.vo.VORangoHora;
import model.vo.VORetardo;
import model.vo.VOServicio;
import model.vo.VOTransbordo;
import model.vo.VOViaje;
import model.vo.VOViajesRuta4C;

public class STSManagerTest {

	STSManager mundo;
	@Before
	public void setUp() throws Exception {
		
		mundo = new STSManager();
		mundo.ITSInit();
		//mundo.ITScargarTR("0410");
		mundo.ITScargarGTFS1C();
		
	}


	@Test
	public void testParteA() throws Exception   {
		
		//	mundo.stopByCoordinates(49.280436, -122.981419);
			IList<VOViaje> temp =  mundo.ITSviajesHuboRetardoParadas1A("C43", "201633333");
			DoubleLinkedList<VOViaje> temp1 = (DoubleLinkedList<VOViaje>) mundo.ITSviajesHuboRetardoParadas1A("C44", "201633333");

			int t = temp1.size();
			int t2 = temp.size();
			
			IList<VOParada> temp2 = mundo.ITSNParadasMasRetardos2A("555555555", 100);
			int size = temp2.size();
			
			IList<VOTransbordo> temp4 = mundo.ITStransbordosRuta3A("C43", "333333333");
			
			int size3 = temp4.size();
			
			IList<VOServicio> temp3 =  mundo.ITSNViajesMasDistancia2C(100, "333333333");
			
			int size2 = temp3.size();
					
			IList<VOViajesRuta4C> temp6 = mundo.ITSParadasCompartidas4C("1888");
			
			LPHashTable<Integer,VOViaje > temp0 = mundo.ITSviajesRetrasoTotalRuta1B("C43", "1888");
			
			int d = temp0.size();
			
			VORangoHora temp12 = mundo.ITSretardoHoraRuta2B("C43", "1888");
			
			DoubleLinkedList<VOViaje> temp15 =  mundo.ITSbuscarViajesParadas3B("1277", "1279", "1888", "5:51:30", "5:56:33");
			
			int hh = temp15.size();
		
	}

//

//	@Test
//	public void testITSviajesRetrasoTotalRuta() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testITSretardoHoraRuta() throws Exception {
//	
//
//		 
//		
//		
//		
//		IList<VOHoraParadaPorParada> temp7 = mundo.ITSViajesPararonEnRango5C("C43", "03:46:20 pm", "06:59:19 pm");
//		
//		int size6 = temp7.size();
//		
//		RedBlackBsT<Integer, IList<VORetardo>> temp5 =  (RedBlackBsT<Integer, IList<VORetardo>>) mundo.retardosViajeFecha3C("9135648", "333333333");
//		
//		
//		int size4 = temp5.size();
		
		
//	}
//
//	@Test
//	public void testITSbuscarViajesParadas() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testParteC() throws Exception {
		
//	}
//
//	@Test
//	public void testITSretardosViaje() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testITSparadasCompartidas() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testITSviajesParadasRangoDeTiemp() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testListStops() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetDistance() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testListaFromCola() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testStopTimeById() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testTripById() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testStopByCoordinates() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testStopByID() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testParadaPorId() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testParsingDate() throws ParseException {
//		Date temp = mundo.parsingDate("03:47:23 pm");
//		Date temp2 = mundo.parsingDate("3:46:40");
//		int temp3 = mundo.getTimeDIference(temp, temp2);
//		
//	
//	
//	}
//
//	@Test
//	public void testGetShape() {
//		fail("Not yet implemented");
//	}

}
