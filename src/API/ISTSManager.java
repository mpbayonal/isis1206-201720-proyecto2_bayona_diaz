package API;

import java.io.FileNotFoundException;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.IRedBlackBST;
import model.data_structures.LPHashTable;
import model.vo.*;

public interface ISTSManager 
{
	/**
	 * inicializa estrucuturas de datos
	 */
	void ITSInit(); 

	/**
	 * Carga toda la informaci�n est�tica necesaria para la operaci�n del sistema.
	 *(Archivo de rutas, viajes, paradas, etc.)
	 * @throws FileNotFoundException 
	 */
	void ITScargarGTFS1C() throws FileNotFoundException;

	/**
	 * Carga la informaci�n en tiempo real de los buses en para fecha determinada.
	 * @param fecha
	 * @throws FileNotFoundException 
	 */
	void ITScargarTR(String fecha) throws FileNotFoundException;

	/**
	 * Retorna una IList de viajes (ordenada por el id), que tuvieron retardo en almenos una parada. 
	 * @param idRuta
	 * @param fecha
	 * @return IList de VOViaje.
	 * @throws FileNotFoundException 
	 * @throws Exception 
	 */
	IList<VOViaje>ITSviajesHuboRetardoParadas1A(String idRuta, String fecha) throws Exception;

	/**
	 * Retorna una lista de las n paradas que tuvieron m�s retardos en la fecha que llega por par�metro.
	 * @param fecha.
	 * @param n.
	 * @return Lista de VOParadas.
	 * @throws Exception 
	 */
	IList<VOParada> ITSNParadasMasRetardos2A(String fecha, int n) throws Exception;

	/**
	 * Retorna una lista, contiene los transbordos.
	 * @param idRuta
	 * @param fecha
	 * @return
	 * @throws Exception 
	 */
	IList<VOTransbordo> ITStransbordosRuta3A(String idRuta, String fecha) throws Exception;


	/**
	 * Retornar una lista con todos los viajes en los que despu�s de un retardo, todas las 
	 * paradas siguientes tuvieron retardo, para una ruta espec�fica en una fecha espec�fica. 
	 * Se debe retornar una lista ordenada por el id del viaje, y la localizaci�n de las 
	 * paradas, de mayor a menor en tiempo de retardo.
	 * @param idRuta
	 * @param fecha
	 * @return
	 * @throws Exception 
	 */
	LPHashTable<Integer, VOViaje> ITSviajesRetrasoTotalRuta1B(String idRuta, String fecha) throws Exception;

	/**
	 * Retorna una lista ordenada con rangos de hora en los que mas retardos hubo.
	 * @param idRuta
	 * @param Fecha
	 * @return
	 * @throws Exception 
	 */
	VORangoHora ITSretardoHoraRuta2B (String idRuta, String fecha ) throws Exception;

	/**
	 * Retorna una lista con los viajes para ir de la parada de inicio a la parada 
	 * final en una fecha y franja horaria determinada.
	 * @param idOrigen
	 * @param idDestino
	 * @param fecha
	 * @param horaInicio
	 * @param horaFin
	 * @return
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	DoubleLinkedList<VOViaje> ITSbuscarViajesParadas3B(String idOrigen, String idDestino, String fecha, String horaInicio, String horaFin) throws NumberFormatException, Exception;

	/**
	 *  Retorna los n viajes que recorrieron m�s distancia en la fecha dada por par�metro.
	 * @param n
	 * @param fecha
	 * @return
	 * @throws Exception 
	 */
	IList<VOServicio> ITSNViajesMasDistancia2C(int n, String fecha) throws Exception;
	
	/**
	 * Retorna un �rbol con los retardos de un viaje con id idViaje en la fecha dada.
	 * @param idViaje
	 * @param fecha
	 * @return
	 * @throws Exception 
	 */
	IRedBlackBST<Integer, IList<VORetardo>> retardosViajeFecha3C(String idViaje, String fecha) throws Exception;
	

	/**
	 * Retorna una lista con todas las paradas del sistema que son compartidas 
	 * por mas de una ruta en una fecha determinada.
	 * @param fecha
	 * @return
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	IList<VOViajesRuta4C> ITSParadasCompartidas4C (String fecha) throws NumberFormatException, Exception;
	
	/**
	 * Retorna los viajes de una ruta dada, que pararon en el rango de tiempo dado.
	 * @param idRuta
	 * @param horaInicio
	 * @param horaFin
	 * @return
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	IList<VOHoraParadaPorParada> ITSViajesPararonEnRango5C(String idRuta, String horaInicio, String horaFin) throws NumberFormatException, Exception;
}