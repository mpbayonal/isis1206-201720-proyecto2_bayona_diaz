package Test.Data_StructuresTest;

import junit.framework.TestCase;
import model.data_structures.LPHashTable;
import model.data_structures.SeparateChainingHashTable;

public class SeparateChainingHashTableTest extends TestCase {

	

		SeparateChainingHashTable<Integer, Double> lpht;
		
		public void setUp1() throws Exception {
			 lpht= new SeparateChainingHashTable<Integer, Double>();
			 for (int i = 0; i < 6; i++) {
				 lpht.put(i, 1.0*i);
				
			}
		}

		public void setUp2() {
			 lpht= new SeparateChainingHashTable<Integer,Double>();
		}
		
		public void testSize() throws Exception {
			setUp1();
			assertEquals("el elemento no es correto", 6, lpht.size());
			setUp2();
			assertEquals("el elemento no es correto", 0, lpht.size());
			
		}
		public void testPut() throws Exception {
			//Agrega un elemto sin superar la capacidad limite.
			setUp1();
			lpht.put(6, 6.0);
			//Agrega un elemento superando la capacidad y haciendo rehash.
			assertEquals("el elemento no es correto", 6.0, lpht.get(6));
			lpht.put(7, 7.0);
			assertEquals("el elemento no es correto", 7.0, lpht.get(7));
			setUp2();
			//Agrega un elemento en una lista vacia
			lpht.put(1, 1.0);
			assertEquals("el elemento no es correto", 1.0, lpht.get(1));
		}
		
		public void testGet() throws Exception {
			setUp1();
			assertEquals("el elemento no es correto", 4.0, lpht.get(4));
			assertEquals("el elemento no es correto", null, lpht.get(8));
			
		}
		public void testDelete() throws Exception {
			setUp1();
			lpht.delete(3);
			assertEquals("el elemento no es correto", 5, lpht.size());
			assertEquals("el elemento no es correto", null, lpht.get(3));
		
		}
	
		
		
		
	

}
